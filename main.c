/**
 * Automatic Guitar Tuner
 *
 * Author: Guyrandy Jean-Gilles
 * License BSD-3-Clause
 *
 * Components:
 *  1 raspberry pi Pico
 *  1 5110 Nokia LCD
 *  1 DC motor
 *  1 h-bridge
 *  3 tactile buttons (all active high)
 *  1 electret microphone
 *
 * Pins for each component in the "#define"s of the source files in this project.
 */
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"
#include "yin.h"
#include "two_wire_motor.h"
#include "pitchSelection.h"
#include "display.h"

// **************** Sound **************** //
#define SAMPLES 128           // number of audio samples to take per FFT computation, must be power of 2
#define SAMPLING_FREQUENCY 4096
#define MIC_ADC 0
#define MIC_GPIO_PIN 26
#define TOLERANCE 3.0
#define LISTEN_BUTTON_PIN 14 // active high

uint16_t micSamplingPeriod;
uint64_t lastSampleTime;
float vReal[SAMPLES]; //create vector of size SAMPLES to hold real values
double vImag[SAMPLES]; //create vector of size SAMPLES to hold imaginary values
void sampleMic(void);
bool poorAudioSignal(void);
bool isListening;

// *************** Motor *************** //
#define MOTOR_PIN1 2
#define MOTOR_PIN2 3

int main(){
    stdio_usb_init();

    // initialize listening button
    gpio_init(LISTEN_BUTTON_PIN);
    gpio_set_dir(LISTEN_BUTTON_PIN, GPIO_IN);
    isListening = false;

    // Initialize ADC
    adc_init();
    adc_gpio_init(MIC_GPIO_PIN);
    adc_select_input(MIC_ADC);

    // Initialize YIN parameters
    Yin yin;
    micSamplingPeriod = round(1000000*(1.0/SAMPLING_FREQUENCY));
    Yin_init(&yin, SAMPLES, .05);

    // Initialize DC motor
    motorInit(MOTOR_PIN1, MOTOR_PIN2);

    pitchSelectionInit();

    displayInit();

    while (1) {
        setTargetFreq();
        setDisplayTargetFreq(getCurrentFreqIdx());
        showDisplay();


        float targetFrequency = getTargetFreq();
        printf("Target Frequency: %lf", targetFrequency);

        // only start to listen if the user presses the listen button
        isListening = gpio_get(LISTEN_BUTTON_PIN);
        if (!isListening){
            stop(MOTOR_PIN1, MOTOR_PIN2);
            printf("Not Listening \n");
            eraseDisplay();
            setDisplayNotListening();
            continue;
        }
        setTargetFreq(); // Do not Repeat Yourself
        setDisplayTargetFreq(getCurrentFreqIdx());
        showDisplay();
        sampleMic();

        float currentFreq = Yin_getPitch(&yin, vReal);
        printf("Frequency: %lf ,", currentFreq);
        if (currentFreq == -1){
            stop(MOTOR_PIN1, MOTOR_PIN2);
            eraseDisplay();
            setDisplayPoorSignal();
            printf("\n");
            continue;
        }
        float freqDelta = currentFreq - targetFrequency;

        // overflatten if string too sharp
        if (currentFreq > targetFrequency){
            float flatFrequency = getFlatFreq();
            while (currentFreq > flatFrequency){
                isListening = gpio_get(LISTEN_BUTTON_PIN); // TODO DO NOT REPEAT YOURSELF
                if (!isListening){
                    stop(MOTOR_PIN1, MOTOR_PIN2);
                    printf("Not Listening \n");
                    eraseDisplay();
                    setDisplayNotListening();
                    continue;
                }

                turn(MOTOR_PIN1, MOTOR_PIN2, true);
                sampleMic();
                currentFreq = Yin_getPitch(&yin, vReal);
                freqDelta = currentFreq - targetFrequency;

                // fix me, do not repeat yourself.
                eraseDisplay();
                setDisplayTargetFreq(getCurrentFreqIdx());
                setDisplayPitchDelta(freqDelta);
                showDisplay();

                printf("Target Frequency: %lf", targetFrequency);
                printf("Frequency: %lf ,", currentFreq);
                printf("Flat Frequency: %lf", flatFrequency);
                printf("\n");
            }
        }

        if (fabs(freqDelta) < TOLERANCE){
            stop(MOTOR_PIN1, MOTOR_PIN2);

            eraseDisplay(); //TODO Do not Repeat Yourself
            setTargetFreq();
            setDisplayTargetFreq(getCurrentFreqIdx());
            setDisplayDoneStatus();
            showDisplay();
            isListening = false;
            printf("Done: ");
            sleep_ms(3000);
            eraseDisplay();
        } else {
            turn(MOTOR_PIN1, MOTOR_PIN2, false);

            eraseDisplay(); //TODO Do not Repeat Yourself
            setDisplayPitchDelta(freqDelta);
            printf("Sharpening, ");
        }
        printf("\n");
    }
}

void sampleMic(void){
  for(uint16_t i=0; i<SAMPLES; i++){
        lastSampleTime = time_us_64();
        vReal[i] = adc_read();
        vImag[i] = 0;
        while(time_us_64() < (lastSampleTime + micSamplingPeriod)){
            // wait until next sampling time if necessary
        }
    }
}
