# Open Source Automatic Guitar Tuner
**_Revision 1 pictured below_**
![CAD Render1](media/CAD_render1.PNG)
![CAD Render2](media/CAD_render2.PNG)
![Physical Automatic Guitar Tuner](media/physical_tuner.jpg)

This project provides source code and 3D-printable files for an automatic guitar tuner based off Band Industries' [Roadie 3](https://www.roadiemusic.com/roadie3). The source code in this project is licensed under the 3-clause BSD licence.

###### As Seen On
[![Hackster IO](media/hackster_logo.jpg)](https://www.hackster.io/news/guyrandy-jean-gilles-raspberry-pi-pico-powered-automated-guitar-tuner-gets-you-pitch-perfect-fast-81c2bbf02b1d)

[![Tom's Hardware](media/tomshardware.png)](https://www.tomshardware.com/news/raspberry-pi-pico-automatic-guitar-tuner)

[![Raspberry Pi](media/Raspberry_Pi_logo.png)](https://www.raspberrypi.org/blog/automatically-tune-your-guitar-with-raspberry-pi-pico/)

[![Hackaday](media/hackaday-logo.png)](https://hackaday.com/2021/09/24/handheld-bot-takes-the-tedium-out-of-guitar-tuning/)

###### Note
1. ~~While the project is functional, I couldn't get the note detection to work perfectly and the motor I used struggled at times to turn my guitar's tuning pegs. Again, it's functional so I have no plans to optimize the project, but maybe you'll find use in what I've done so far.~~ I'm actively working on a second revision. Find the first iteration here: https://gitlab.com/guyjeangilles/automatic-guitar-tuner/-/tree/b91588dfc0f9ad3a285ed1cc72bed9c4d2ef4cef

###### Changes in Revision 2
- [X] Alternate tunings
- [X] Always tune strings up to their target frequency
- [ ] Optional 1/4" input

###### TODO
* ~~Overflatten peg, then tighten up when a string is too tight~~
* ~~Add real frequencies~~
* ~~Infinite throw with the 8-switch~~
* ~~replace LEDs with a display~~
* ~~Show sharp and flat symbols on LCD~~
* Make Carrier board schematics
* Design enclosure

###### Note
[The Raspberry Pi Pico SDK](https://github.com/raspberrypi/pico-sdk) is a submodule of this repo. Access to the SDK is required to build this project.

## Components
1. [Raspberry Pi Pico](https://www.raspberrypi.org/products/raspberry-pi-pico/)
1. [GM11a DC motor](https://solarbotics.com/product/gm11a/)
1. [Electret Microhpone with MAX4466 adjustable gain](https://www.adafruit.com/?q=Electret%20Microphone%20Amplifier%20-%20MAX4466%20with%20Adjustable%20Gain)
1. [L293D Dual H-Bridge](https://www.adafruit.com/?q=Dual%20H-Bridge%20Motor%20Driver%20for%20DC%20or%20Steppers%20-%20600mA%20-%20L293D)
1. [12mm x 10mm x 6mm Tactile Button](https://www.adafruit.com/product/1119)
1. [Two 6mm x 6mm tactile buttons](https://www.adafruit.com/product/367)
1. [Adafruit Powerboost 1000c](https://www.adafruit.com/product/2465)
1. [Nokia 5110 Liquid Crystal Display](https://www.sparkfun.com/products/10168)

## Use Instructions
Push the "pitch increase" or "pitch decrease" buttons to select the target note. The corresponding note will appear on the LCD screen. Once the desired tone is selected, place the tuner's head on the guitar's tuning peg. Pluck the desired string then press **and** hold the push button to enabled the microphone. If the signal quality is good, a "#" or "*b*" symbol will appear on the LCD with a corresponding bar indicating how out of tune the string is. Wait until "Done" is displayed on the LCD. The string is now tuned.
