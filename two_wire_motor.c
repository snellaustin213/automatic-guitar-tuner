/** Raspberry Pi Pico library for 2-wire dc motor
 * Last modified by Guyrandy Jean-Gilles
 * License BSD-3-Clause
 */
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "two_wire_motor.h"

void motorInit(uint8_t pin1, uint8_t pin2){
    gpio_init(pin1);
    gpio_init(pin2);

    gpio_set_dir(pin1, GPIO_OUT);
    gpio_set_dir(pin2, GPIO_OUT);
}

// TODO: implement speed via PWM enable PIN
void turn(uint8_t pin1, uint8_t pin2, bool clockwise){
    gpio_put(pin1, clockwise);
    gpio_put(pin2, !clockwise);
}

void stop(uint8_t pin1, uint8_t pin2){
    gpio_put(pin1, 0);
    gpio_put(pin2, 0);
}
