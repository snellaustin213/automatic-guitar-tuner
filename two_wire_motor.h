#ifndef TWO_WIRE_MOTOR_H_INCLUDED
#define TWO_WIRE_MOTOR_H_INCLUDED

void motorInit(uint8_t pin1, uint8_t pin2);
void turn(uint8_t pin1, uint8_t pin2, bool clockwise);
void stop(uint8_t pin1, uint8_t pin2);
#endif // TWO_WIRE_MOTOR_H_INCLUDED
